import React from 'react';
import logo from './logo.svg';
import './App.css';
import DatosTransmision from './components/DatosTransmision';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <DatosTransmision/>
      </header>
    </div>
  );
}

export default App;
