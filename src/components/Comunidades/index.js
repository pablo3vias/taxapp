import React, { Component } from 'react';
import {todascomunidades} from '../../comunidadesAutonomas.json';

class Comunidades extends Component{
    constructor(){
        super(); 
        this.state = {
            comunidadSeleccionada: {
                valor: '',
                impuesto: '',
                excepcion: ''
            },
            todascomunidades
        };
        this.handleInput = this.handleInput.bind(this);
    }

    handleInput(e){
        
        let comunidadSeleccionada = {...this.state.comunidadSeleccionada};
        const {value,selectedIndex,name} = e.target;
        comunidadSeleccionada = this.state.todascomunidades[selectedIndex];
        this.setState({
          [name]: comunidadSeleccionada,
        });
        this.props.onAddTodo(this.state.comunidadSeleccionada);
    }

    render() {
        let comunidadesAutonomas = this.state.todascomunidades;
        let options = comunidadesAutonomas.map((c) =>
        <option key={c.valor}>{c.valor}</option>)
        return (
            <div className="form-group row">
                <label className="col-sm-4 col-form-label">Comunidad Autónoma *</label>
                <div className="col-sm-8">
                    <select name="comunidadSeleccionada" id="comunidadSeleccionada" className="form-control" onChange={this.handleInput}>
                        {options}
                    </select>
                </div>
            </div>
        );
    }
}

export default Comunidades