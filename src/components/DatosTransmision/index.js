import React, { Component } from 'react';
import moment from 'moment';

import {todascomunidades} from '../../comunidadesAutonomas.json';
import {modelos} from '../../modelos.json';
import {marcas} from '../../marcas.json';
import {porcentajes} from '../../porcentajeDepreciacion.json';

import Resultado from '../Resultado';

class DatosTransmision extends Component{
    constructor(){
        super(); 
        this.state = {
            comunidad: '',
            fechaMatriculacion: '',
            marca: '',
            modelo: '',
            todascomunidades,
            modelos,
            marcas,
            porcentajes,
            dateObject: moment(),
            resultado: ''
        };
        this.handleInput = this.handleInput.bind(this);
        this.vaciarCampos = this.vaciarCampos.bind(this);
        this.calcular = this.calcular.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    
    

    handleChange(date) {
        this.setState({
          startDate: date
        });
      }

    handleInput(e){
        const {value , name} = e.target;
        
        this.setState({
            [name]: value,
        });
        
        if (name === 'comunidad' && value === ''){
            document.getElementById('fechaMatriculacion').value = '';
            document.getElementById('marca').value = '';
            document.getElementById('modelo').value = '';
            this.setState({
                fechaMatriculacion: '',
                marca: '',
                modelo: ''
            });
        }
        else if (name === 'fechaMatriculacion' && value === ''){
            document.getElementById('marca').value = '';
            document.getElementById('modelo').value = '';
            this.setState({
                marca: '',
                modelo: ''
            });
        }
        else if (name === 'marca' && value === ''){
            document.getElementById('modelo').value = '';
            this.setState({
                modelo: ''
            });
        }
    }

    vaciarCampos(e){
        document.getElementById('comunidad').value = '';
        document.getElementById('fechaMatriculacion').value = '';
        document.getElementById('marca').value = '';
        document.getElementById('modelo').value = '';
        document.getElementById('tbComunidad').innerHTML = '';
        document.getElementById('tbvFiscal').innerHTML = '';
        document.getElementById('tbPorcentaje').innerHTML = '';
        document.getElementById('tbvFinal').innerHTML = '';
        this.setState({
            comunidad: '',
            fechaMatriculacion: '',
            marca: '',
            modelo: ''
        });
    }

    fechaActual = () => {
        return this.state.dateObject.toDate();
    };

    numeroAnios = () => {
        return moment(this.fechaActual()).diff(moment(this.state.fechaMatriculacion),'years');
    };

    porcentaje = () =>{
        let aniosCoche = this.numeroAnios();
        let porcentajes = this.state.porcentajes;
        let p = porcentajes.filter(function(c){
            if (c.anios == aniosCoche) return c.porcentaje;
        });
        if(p.length > 0) return p[0].porcentaje;
        return 0.10;
    }

    calcular(e){
        let resultado = 0.0;
        let modeloCoche = this.state.modelo;
        let modelos = this.state.modelos;
        let coche = {};
        for(var i=0; i<modelos.length; i++){
            if(modelos[i].modelo == modeloCoche){
                coche = modelos[i]
            }
        }
        var porcentaje = this.porcentaje();
        console.log(porcentaje);
        resultado = coche.valor * porcentaje;
        
        if(this.state.comunidad == 'Asturias'){
            if (coche.cvf > 15){
                resultado = resultado * 0.08;
            }
            else{
                resultado = resultado * 0.04;
            }
        }
        else if(this.state.comunidad == 'Castilla y León'){
            if (coche.cvf > 15){
                resultado = resultado * 0.08;
            }
            else{
                resultado = resultado * 0.05;
            }
        }
        this.state.resultado = resultado;
        console.log(document.getElementById('tbComunidad'))

        document.getElementById('tbComunidad').innerHTML = this.state.comunidad;
        document.getElementById('tbvFiscal').innerHTML = coche.valor;
        document.getElementById('tbPorcentaje').innerHTML = Math.floor(porcentaje * 100) + '%';
        document.getElementById('tbvFinal').innerHTML = resultado;
    }

    render() {
        let comunidadesAutonomas = this.state.todascomunidades;
        let listaComunidades = comunidadesAutonomas.map((c) =>
        <option key={c.valor}>{c.valor}</option>)

        let marcas = this.state.marcas;
        let listaMarcas = marcas.map((c) =>
        <option key={c.id}>{c.valor}</option>)
        
        let modelos = this.state.modelos;
        let marcaSeleccionada = this.state.marca;
        let listaModelos = modelos.map(function(c){
            if (c.marca == marcaSeleccionada) return <option key={c.modelo}>{c.modelo}</option>
        });
        
        return (
            <div className="Selectbox card">
                <div className="card-header text-secondary">
                    Datos de la transmisión
                </div>
                <form className="card-body text-secondary">
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Comunidad *</label>
                        <div className="col-sm-8">
                            <select name="comunidad" id="comunidad" className="form-control" onChange={this.handleInput}>
                                <option></option>
                                {listaComunidades}
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Fecha de Matriculación *</label>
                        <div className="col-sm-8">
                            <input name="fechaMatriculacion" id="fechaMatriculacion" className="form-control" onChange={this.handleInput} disabled={!this.state.comunidad} placeholder="AAAA-MM-DD"></input>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Fabricante/Marca *</label>
                        <div className="col-sm-8">
                            <select name="marca" id="marca" className="form-control" onChange={this.handleInput} disabled={!this.state.fechaMatriculacion}>
                                <option></option>
                                {listaMarcas}
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Modelo *</label>
                        <div className="col-sm-8">
                            <select name="modelo" id="modelo" className="form-control" onChange={this.handleInput} disabled={!this.state.marca}>
                                <option></option>
                                {listaModelos}
                            </select>
                        </div>
                    </div>
                </form>
                <div className="card-footer text-secondary">
                    <button className="btn btn-success float-left" onClick={this.vaciarCampos}>Nueva Consulta</button>
                    <button className="btn btn-success float-right" onClick={this.calcular}>Calcular</button>
                </div>
                <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Comunidad</th>
                        <th scope="col">Valor Fiscal</th>
                        <th scope="col">Corrector de Antiguedad(%)</th>
                        <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="tbComunidad"></td>
                        <td id="tbvFiscal"></td>
                        <td id="tbPorcentaje"></td>
                        <td id="tbvFinal"></td>
                    </tr>
                </tbody>
            </table>
            </div>
        );
    }
}

export default DatosTransmision