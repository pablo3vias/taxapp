import React, { Component } from 'react';

class Resultado extends Component {
    constructor() {
        super();
        this.state = {
            comunidad: '',
            resultado: '',
            valorFiscal: '',
            corrector: ''
        };

    }

    render() {
        
        let comunidad = this.props.comunidad;
        let vfiscal = this.props.vfiscal;
        let porcentaje = this.props.porcentaje;
        let vfinal = this.props.vfinal;
        return (

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Comunidad</th>
                        <th scope="col">Valor Fiscal</th>
                        <th scope="col">Corrector de Antiguedad(%)</th>
                        <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{comunidad}</td>
                        <td>{vfiscal}</td>
                        <td>{porcentaje}%</td>
                        <td>{vfinal}</td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default Resultado